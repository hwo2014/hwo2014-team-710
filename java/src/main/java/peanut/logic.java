/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package peanut;

/**
 *
 * @author Trung
 */
import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import java.util.Arrays;
import java.util.ArrayList;



public class logic {
   
    double power;
    int lap = 1;
    int turboStart;
    double trueR;
    TestData testData = new TestData();
    Physic physic =  new Physic();
    
    String decision="0";
    int tick = 0;
    int lasttick = 0;
    RaceData racedata;
    CarPosition[] carPosition;
    boolean turboAvai = false;
    TurboStat turbo;
    CarPosition[] lastPosition;
    CarPosition[] curPosition;  
    
    double lastSpeed[];
    double lastAcc[];
   
    
   
    
    double curSpeed[];
    double curAcc[];
    
    int ncar;
    int hero = 0;
    
    double testpower = 0.7;
    boolean test = false;
    PredictData expect;
    logic() {
    }

    private void updategameinit() {       
        
        
        
        for (RaceData.Race.Track.Piece piece : racedata.race.track.pieces) {
            piece.lenghtOfLane = new double[racedata.race.track.lanes.length];
                    
            if (piece.length == 0) {
                piece.length = piece.radius*Math.abs(piece.angle) * Math.PI * 2 / 360;            
            
            for (int i = 0; i<piece.lenghtOfLane.length;i++) {
                piece.lenghtOfLane[i] = Math.abs((Math.abs(piece.angle)/piece.angle*piece.radius-racedata.race.track.lanes[i].distanceFromCenter))*Math.PI*2*Math.abs(piece.angle)/360;
            }
        } else {
             for (int i = 0; i<piece.lenghtOfLane.length;i++) {
                 piece.lenghtOfLane[i] = piece.length;
             }
            }
            
            
        }
        
    }
    
    private void updatePosition1st() {
        ncar = carPosition.length;
        for (int i =1;i<ncar;i++) {
            if (carPosition[i].id.name.equals("peanut")) {
                hero = i;
            }
        }
        curPosition = new CarPosition[ncar];
        lastPosition = new CarPosition[ncar];
        lastSpeed = new double[ncar];
        lastAcc = new double[ncar];       
        curSpeed = new double[ncar];
        curAcc = new double[ncar];
        
        
        
       
        
        for(int i =0;i<ncar;i++) {
            lastPosition[i] = new CarPosition();    
            curPosition[i] =new CarPosition();  
        }       
        
        }
    

    private void updatePosition() {
       
        

        
       
        double travelDist = 0;
        double cumDist = 0;
        if (!test){
            if (curPosition[hero].piecePosition.pieceIndex ==0 && lastPosition[hero].piecePosition.pieceIndex  !=0) {
            lap++;
        }
        if (lap == racedata.race.raceSession.laps) {
            if (curPosition[hero].piecePosition.pieceIndex > 0) {
                racedata.race.track.pieces[curPosition[hero].piecePosition.pieceIndex-1].radius =0;
            }
        }
        }
        
        lastPosition = Arrays.copyOf(curPosition, curPosition.length);
        curPosition = Arrays.copyOf(carPosition, carPosition.length);
         for (int i = 0; i<ncar; i++) {
        
       
        if (lastPosition[i].piecePosition.pieceIndex !=curPosition[i].piecePosition.pieceIndex) {
         if (curPosition[i].piecePosition.pieceIndex == 0) {
             curPosition[i].piecePosition.pieceIndex = racedata.race.track.pieces.length;
         }   
         for (int j = lastPosition[i].piecePosition.pieceIndex; j<curPosition[i].piecePosition.pieceIndex;j++){
         cumDist += racedata.race.track.pieces[j].lenghtOfLane[lastPosition[i].piecePosition.lane.startLaneIndex];
         }
         if (curPosition[i].piecePosition.pieceIndex == racedata.race.track.pieces.length) {
             curPosition[i].piecePosition.pieceIndex = 0;
         }   
         }
        travelDist = curPosition[i].piecePosition.inPieceDistance-lastPosition[i].piecePosition.inPieceDistance+cumDist;
        
        lastSpeed[i] = curSpeed[i];
        curSpeed[i] = travelDist / (tick - lasttick);
        lastAcc[i] = curAcc[i];
        curAcc[i] = (curSpeed[i] - lastSpeed[i]) / (tick - lasttick);
       
        }
       
        
        
        if (tick==2) {
            testData.linear[0] = new TestDataLinear(Double.parseDouble(decision),curSpeed[hero],curAcc[hero]);
        }
        if (tick==3) {
            testData.linear[1] = new TestDataLinear(Double.parseDouble(decision),curSpeed[hero],curAcc[hero]);
        }
        if (tick ==4 ) {
            physic.updatePhysic(testData);
            System.out.println(physic.lk1 + "\t" + physic.lk2);
        }
        
        
        
     
            
        
           
        
        
            
     
       
        
  
    }
    
    private PredictData predict(double p, CarPosition position, double speedt1, double acct1){
        double acct = physic.lk1*p+physic.lk2*speedt1;
        double speedt = speedt1+acct;        
     
      
      
        CarPosition newPosition = new CarPosition();
        newPosition.angle = position.angle;
        newPosition.id = position.id;
        newPosition.lap = position.lap;
        newPosition.piecePosition.lane = position.piecePosition.lane;
        newPosition.piecePosition.pieceIndex = position.piecePosition.pieceIndex;
        newPosition.piecePosition.inPieceDistance = position.piecePosition.inPieceDistance + speedt;
        if (newPosition.piecePosition.inPieceDistance  > racedata.race.track.pieces[position.piecePosition.pieceIndex].lenghtOfLane[position.piecePosition.lane.endLaneIndex]) {
            newPosition.piecePosition.inPieceDistance = newPosition.piecePosition.inPieceDistance-racedata.race.track.pieces[position.piecePosition.pieceIndex].lenghtOfLane[position.piecePosition.lane.endLaneIndex];
            newPosition.piecePosition.pieceIndex = newPosition.piecePosition.pieceIndex+1;
            if (newPosition.piecePosition.pieceIndex==racedata.race.track.pieces.length){
                newPosition.piecePosition.pieceIndex = 0;
            }
        }
        return new PredictData(newPosition,speedt,acct);
        
    }
    
    private void updateraw(MsgWrapper msg) {
        /* update raw data from server message*/
        if (tick != msg.gameTick){
        lasttick = tick;        
        tick = msg.gameTick;
        
        }
        String data = msg.data.toString();

        Gson gson = new Gson();

        if (msg.msgType.equals("gameInit")) {
            
            racedata = gson.fromJson(data, RaceData.class);
            if (data.contains("durationMs")) {
                test = true;
                
            } else {
                if (test) {
                    testpower = testpower ;
                } else {
                    testpower = 0.5;
                }
                test = false;
                
             
            }
            System.out.println(data);
            updategameinit();
            
        } else if (msg.msgType.equals("carPositions")) {
            carPosition = gson.fromJson(data, CarPosition[].class);
          
            if (msg.gameTick == 0) {
                updatePosition1st();
            } else {
            updatePosition(); 
            decide();
            }
        }
         else if (msg.msgType.equals("turboAvailable")) {
            turboAvai = true;
            turbo = gson.fromJson(data, TurboStat.class);
            
        } else if (msg.msgType.equals("crash")) {
                      testpower = testpower*0.85;
            
   
    lastSpeed[hero]= 0;
    lastAcc[hero]= 0;
    
     curSpeed[hero]= 0;
    curAcc[hero]= 0;
    
            
        } else if (msg.msgType.equals("gameEnd")) {
                    
                   System.out.println(physic.lk1 +"\t"+physic.lk2 + "\t" + testpower);
            
        }
    }

    public void updatestage(MsgWrapper msg) {
        /*update the game stage arrays, then run the decide method to get new decision*/
        updateraw(msg);

       

    }

    private void decide() {
        /* make decision based on the current game stage arrays*/
        
        System.out.println(tick +"\t"+ predict(power, lastPosition[hero],lastSpeed[hero],lastAcc[hero]).speed +"\t"+curSpeed[hero] + "\t" + decision);
        power = testpower;
        if (curSpeed[hero]> -physic.lk1*testpower/physic.lk2) {
            power = 0;
        }
        decision = Double.toString(power);
        if (!test) {
            if (racedata.race.track.pieces[curPosition[hero].piecePosition.pieceIndex].radius ==0 ||racedata.race.track.pieces[curPosition[hero].piecePosition.pieceIndex].radius >=250) {
                PredictData predict1 = predict(1.0, curPosition[hero],curSpeed[hero],curAcc[hero]);
                while((racedata.race.track.pieces[predict1.position.piecePosition.pieceIndex].radius ==0||racedata.race.track.pieces[predict1.position.piecePosition.pieceIndex].radius >=250) && predict1.speed>3) {
                    predict1 = predict(0,predict1.position,predict1.speed,predict1.acc);                
                }
                if (predict1.speed < -testpower*physic.lk1/physic.lk2) {
                    this.decision = "1.0";  
                    power = 1.0;
                } else{
                    this.decision ="0.0";
                    power = 0.0;
                }
                if(turboAvai) {
                    int p1 = predict1.position.piecePosition.pieceIndex;
                    int p2 = p1+1;
                    if (p2==racedata.race.track.pieces.length) {
                        p2 = 0;
                    }
                    int  p3 = p2+1;
                     if (p3==racedata.race.track.pieces.length) {
                        p3 = 0;
                    }
                   
                    
               /* if ((racedata.race.track.pieces[p4].radius ==0 || racedata.race.track.pieces[p4].radius >=200)&&(racedata.race.track.pieces[p1].radius ==0 || racedata.race.track.pieces[p1].radius >=200)&&(racedata.race.track.pieces[p2].radius ==0 || racedata.race.track.pieces[p2].radius >=200)&&(racedata.race.track.pieces[p3].radius ==0 || racedata.race.track.pieces[p3].radius >=200)) {*/
                     if(racedata.race.track.pieces[p1].radius ==0 &&racedata.race.track.pieces[p2].radius ==0&&racedata.race.track.pieces[p3].radius ==0) {
                    this.decision="turbo";
                    turboAvai = false;
                    physic.lk1 = turbo.turboFactor*physic.lk1;
                    testpower = testpower/turbo.turboFactor;
                    
                    turboStart = tick;
                }
                    }
                if (turboStart!=0 && tick == turboStart+turbo.turboDurationTicks) {
                    physic.lk1= physic.lk1/turbo.turboFactor;
                    testpower = testpower*turbo.turboFactor;
                }
                
                
            }
            
            int predictpiece = predict(1.0, curPosition[hero],curSpeed[hero],curAcc[hero]).position.piecePosition.pieceIndex;
            if (predictpiece != curPosition[hero].piecePosition.pieceIndex) {
                if (racedata.race.track.pieces[predict(1.0, curPosition[hero],curSpeed[hero],curAcc[hero]).position.piecePosition.pieceIndex].isSwitch){
                    boolean blockedleft = false;
                    boolean blockedright = false;
                    boolean blockedfront = false;
                    if (curPosition[hero].piecePosition.lane.endLaneIndex == 0) {
                        blockedleft = true;
                    }
                     if (curPosition[hero].piecePosition.lane.endLaneIndex == racedata.race.track.lanes.length-1) {
                        blockedright = true;
                    }
                    for (int i = 0; i<curPosition.length;i++) {
                        if (i!=hero) {
                            if (curPosition[i].piecePosition.pieceIndex == predictpiece){
                                if (curPosition[i].piecePosition.lane.endLaneIndex ==curPosition[hero].piecePosition.lane.endLaneIndex+1){
                                    blockedright = true;
                                }
                                if (curPosition[i].piecePosition.lane.endLaneIndex ==curPosition[hero].piecePosition.lane.endLaneIndex){
                                    blockedfront = true;
                                }
                                if (curPosition[i].piecePosition.lane.endLaneIndex ==curPosition[hero].piecePosition.lane.endLaneIndex-1){
                                    blockedleft = true;
                                }
                                    
                            }
                        }
                        
                    }
                    if (!blockedright) {
                        decision = "Right";
                    } else if(blockedfront && !blockedleft) {
                        decision ="Left";
                    }
                    
                }
            } 
        
       
        } 
        
      
    }
    

    public String getDecision() {

        return this.decision;
    }
}

class PredictData {
    public CarPosition position;
    public double anglespeed,speed,acc;
    
    public PredictData(CarPosition position, double speed, double acc) {
        this.position = position;
        this.anglespeed = anglespeed;
        this.speed = speed;
        this.acc = acc;
    }
}

class RaceData {

    public Race race;

    class Race {
        
        public RaceSession raceSession ;
        
        class RaceSession {
            int laps;
        }

        public Track track;
        
        

        class Track {

            public String id;
            public Piece[] pieces;
            public Lane[] lanes;
            
            /*public Car[] cars;*/

           class Piece {

                public double length = 0;
                public double radius = 0;
                public double angle = 0;
                @SerializedName("switch")
                public boolean isSwitch = false;
                public double[] lenghtOfLane;

            }

            class Lane {

                public int distanceFromCenter;
                public int index;
            }
            
         
            
          
        }
    }
}

class CarPosition {

    public CarId id;
    public double angle;
    public PiecePosition piecePosition;
    public int lap;
    
    public CarPosition(){
        this.id = new CarId();
        this.angle = 0;
        this.piecePosition = new PiecePosition();
        this. lap = 0;
    }
    
    class CarId {

                   public String name;
                   public String color;
                    
                    public CarId() {
                        this.name ="";
                        this.color ="";
                    }
                }
    class PiecePosition {

        public int pieceIndex;
        public double inPieceDistance;
        public LaneSwitch lane;

        class LaneSwitch {

            public int startLaneIndex;
            public int endLaneIndex;
            
            public LaneSwitch() {
                this.startLaneIndex = 0;
                this.endLaneIndex =0;
            }            
        }
        
        public PiecePosition() {
            this.pieceIndex = 0;
            this.inPieceDistance = 0;
            this.lane = new LaneSwitch();
        }        
        
    }
}

class TurboStat {

    public int turboDurationTicks = 0;
    public double turboFactor = 0;
}

class Physic {
    public double lk1;
    public double lk2;
   
    public Physic(){
        lk1 = 0.2;
        lk2 = -0.02;
    }
        
    public void updatePhysic(TestData data) {
       lk1 = (data.linear[1].a-data.linear[0].a*data.linear[1].v/data.linear[0].v)/(data.linear[1].p-data.linear[0].p*data.linear[1].v/data.linear[0].v);
        lk2 = (data.linear[1].a-lk1*data.linear[1].p)/data.linear[1].v;
       
        
        
      
        
        
    }
    
    
}

class TestData {
    public TestDataLinear[] linear;

    
    
    public TestData() {
        linear = new TestDataLinear[2];
       
       
    }
}

class TestDataLinear {
    public double p;
    public double v;
    public double a;

    
    
    public TestDataLinear(double p, double v, double a) {
        this.p = p;
        this.v = v;
        this.a = a;
       
    }
}
