package peanut;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.*;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        /*String cmd = "join";
        if (args.length == 5) {
            cmd = args[4];        
        }*/
        String cmd =(args.length == 5)?args[4]:"join";
        String pass = "tetra";
        String track = "france";
        int cc = 1;        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        botId botid = new botId(botName,botKey);
        
        new Main(reader, writer, new Join(botName, botKey), new CreateRace(botid, track, pass, cc), new JoinRace(botid, track,pass, cc), cmd);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join, final CreateRace createRace, final JoinRace joinRace, final String cmd) throws IOException {
        this.writer = writer;
        String line = null;    
        
       logic ai = new logic();         
       if (cmd.equals("join")) {
           send(join);
       } else if (cmd.equals("createrace")) { 
           send(createRace);
       } else if (cmd.equals("joinrace")) {
           send(joinRace);
       } else {           
           System.out.println("Unknown command. Restart the bot.");
       }        

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
            
            if (msgFromServer.msgType.equals("carPositions")) { 
                ai.updatestage(msgFromServer);
                /*System.out.println(ai.tick+":"+ai.carPosition[0].piecePosition.pieceIndex + ":" + ai.carPosition[0].piecePosition.inPieceDistance);*/
                if (ai.getDecision().equals("Left")) {
                    send(new SwitchLane("Left",ai.tick));
                } else if (ai.getDecision().equals("Right")) {
                    send(new SwitchLane("Right",ai.tick));                    
                } else if (ai.getDecision().equals("turbo")) {
                    send(new Turbo(ai.tick));
                } else {
                    send(new Throttle(Double.parseDouble(ai.getDecision()),ai.tick));
                    
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {                
                System.out.println("Race init");  
                ai.updatestage(msgFromServer);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                ai.updatestage(msgFromServer);
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Throttle(1.0,0));
            } else if (msgFromServer.msgType.equals("turboAvailable")) {    
                ai.updatestage(msgFromServer);
                System.out.println("Turbo available");                
            } else if (msgFromServer.msgType.equals("crash")) {    
                ai.updatestage(msgFromServer);
                System.out.println("Crash");                
            }
            else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        
        writer.flush();
        
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    protected  int gameTick() {
        return 0;
    }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public final int gameTick ;

    MsgWrapper(final String msgType, final Object data, final int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData(),sendMsg.gameTick());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;
    
    

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
    
   
   
    
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
    
    @Override
    protected int gameTick() {
        return 0;
    }
}

class Throttle extends SendMsg {
    private double value;
    private int tick;

    public Throttle(double value, int tick) {
        this.value = value;
        this.tick = tick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
    
     @Override
    protected int gameTick() {
        return tick;
    }
}
class CreateRace extends SendMsg {
    public final Object botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    CreateRace(final Object botId, final String trackName, final String password, final int carCount) {
        this.botId = botId;     
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;        
    }    
    
     @Override
    protected Object msgData() {
        return this;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class JoinRace extends SendMsg {
    public final Object botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    JoinRace(final Object botId, final String trackName, final String password, final int carCount) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }
    
    @Override
    protected Object msgData() {
        return this;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}


class botId {
    public String name;
    public String key;
    botId(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class SwitchLane extends SendMsg {
    public String value;
    public int tick;

    SwitchLane(String direction, int tick) {
        this.value = direction;
        this.tick = tick;
       
    }
    
    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
      @Override
    protected int gameTick() {
        return tick;
    }
}

class Turbo extends SendMsg {
    private String value;
    private int tick;

    public Turbo(int tick) {
        this.value = "Turbo activated";
        this.tick = tick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    protected int gameTick() {
        return tick;
    }
}
